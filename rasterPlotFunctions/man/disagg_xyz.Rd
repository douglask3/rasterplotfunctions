\name{disagg_xyz}
\alias{disagg_xyz}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
disagg_xyz(x, y, z, rm.na = FALSE, smooth_factor = 5)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{x}{
%%     ~~Describe \code{x} here~~
}
  \item{y}{
%%     ~~Describe \code{y} here~~
}
  \item{z}{
%%     ~~Describe \code{z} here~~
}
  \item{rm.na}{
%%     ~~Describe \code{rm.na} here~~
}
  \item{smooth_factor}{
%%     ~~Describe \code{smooth_factor} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (x, y, z, rm.na = FALSE, smooth_factor = 5) 
{
    z = rasterFromXYZ(cbind(x, y, z))
    z = disaggregate(z, smooth_factor, method = "bilinear")
    xy = xyFromCell(z, 1:length(values(z)))
    z = values(z)
    if (rm.na) {
        test = is.na(z) == FALSE
        xy = xy[test, ]
        z = z[test]
    }
    return(list(xy[, 1], xy[, 2], z))
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
