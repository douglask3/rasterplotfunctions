\name{pattern.raster}
\alias{pattern.raster}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
pattern.raster(r, pattern = "forward-diagonal", res = 4, thick = 0.7, mult = TRUE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{r}{
%%     ~~Describe \code{r} here~~
}
  \item{pattern}{
%%     ~~Describe \code{pattern} here~~
}
  \item{res}{
%%     ~~Describe \code{res} here~~
}
  \item{thick}{
%%     ~~Describe \code{thick} here~~
}
  \item{mult}{
%%     ~~Describe \code{mult} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (r, pattern = "forward-diagonal", res = 4, thick = 0.7, 
    mult = TRUE) 
{
    round.p1.d2 <- function(r, invert = FALSE) {
        r = (r + 1)/2
        thick = 1 - thick
        r[r < thick] = 0
        r[r >= thick] = 1
        if (invert) 
            r = 1 - r
        return(r)
    }
    a = r
    if (pattern == "check.board") 
        values(a) = round.p1.d2(cos(res * xFromCell(a, 1:length(a))) * 
            cos(res * yFromCell(a, 1:length(a))))
    if (pattern == "Diamond") 
        values(a) = round.p1.d2(cos(res * xFromCell(a, 1:length(a))) + 
            cos(res * yFromCell(a, 1:length(a))))
    if (pattern == "Circle") {
        thick = 1 - 0.3 * thick
        values(a) = round.p1.d2(cos(res * xFromCell(a, 1:length(a))) + 
            cos(res * yFromCell(a, 1:length(a))), TRUE)
    }
    if (pattern == "romulan") 
        values(a) = round.p1.d2(cos(res * yFromCell(a, 1:length(a)) * 
            sin(res * xFromCell(a, 1:length(a)))))
    if (pattern == "circles-spirals") 
        values(a) = round.p1.d2(cos(res * yFromCell(a, 1:length(a)) * 
            res * xFromCell(a, 1:length(a))))
    if (pattern == "forward-diagonal") 
        values(a) = round.p1.d2(cos(res * yFromCell(a, 1:length(a)) + 
            res * xFromCell(a, rev(1:length(a)))))
    if (pattern == "backward-diagonal") 
        values(a) = round.p1.d2(cos(res * yFromCell(a, 1:length(a)) - 
            res * xFromCell(a, rev(1:length(a)))))
    if (pattern == "horizontal") 
        values(a) = round.p1.d2(cos(res * xFromCell(a, 1:length(a))))
    if (pattern == "vertical") 
        values(a) = round.p1.d2(cos(res * yFromCell(a, 1:length(a))))
    if (mult) 
        a = r * a
    return(a)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
