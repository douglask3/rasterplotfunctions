\name{add_coastline}
\alias{add_coastline}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
add_coastline(coastline = NULL, projection = NULL, orientation = NULL, lwd.coast = 1, x_range = c(-180, 180), y_range = c(-90, 90), fill.ocean = TRUE, rast, ocean_col = "white", map_db = "world", ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{coastline}{
%%     ~~Describe \code{coastline} here~~
}
  \item{projection}{
%%     ~~Describe \code{projection} here~~
}
  \item{orientation}{
%%     ~~Describe \code{orientation} here~~
}
  \item{lwd.coast}{
%%     ~~Describe \code{lwd.coast} here~~
}
  \item{x_range}{
%%     ~~Describe \code{x_range} here~~
}
  \item{y_range}{
%%     ~~Describe \code{y_range} here~~
}
  \item{fill.ocean}{
%%     ~~Describe \code{fill.ocean} here~~
}
  \item{rast}{
%%     ~~Describe \code{rast} here~~
}
  \item{ocean_col}{
%%     ~~Describe \code{ocean_col} here~~
}
  \item{map_db}{
%%     ~~Describe \code{map_db} here~~
}
  \item{\dots}{
%%     ~~Describe \code{\dots} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (coastline = NULL, projection = NULL, orientation = NULL, 
    lwd.coast = 1, x_range = c(-180, 180), y_range = c(-90, 90), 
    fill.ocean = TRUE, rast, ocean_col = "white", map_db = "world", 
    ...) 
{
    if (is.null(coastline)) {
        if (fill.ocean) {
            outline <- map(map_db, regions = "Australia", exact = TRUE, 
                plot = FALSE)
            xrange <- range(outline$x, na.rm = TRUE)
            yrange <- range(outline$y, na.rm = TRUE)
            xbox <- xrange + c(-1, 1)
            ybox <- yrange + c(-1, 1)
            subset <- !is.na(outline$x)
            polypath(c(outline$x[subset], NA, c(xbox, rev(xbox))), 
                c(outline$y[subset], NA, rep(ybox, each = 2)), 
                col = "white", border = "white", rule = "evenodd")
        }
        if (is.null(projection)) {
            try(map(map_db, add = TRUE, interior = FALSE, xlim = x_range, 
                ylim = y_range, ...), silent = TRUE)
        }
        else {
            try(map(map_db, add = TRUE, interior = FALSE, projection = projection, 
                orientation = orientation, xlim = x_range, ylim = y_range, 
                ...), silent = TRUE)
        }
    }
    else {
        try(add_coast(coastline, lwd = lwd.coast), silent = TRUE)
    }
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
